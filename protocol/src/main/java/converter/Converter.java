package converter;

import com.google.gson.Gson;
import message.Message;

/**
 * Created on 24.04.2017.
 */
public class Converter {
    private static final Gson gson = new Gson();

    public static String toJson(Message message) {
        return gson.toJson(message);
    }

    public static Message fromJson(String json) {
        return gson.fromJson(json, Message.class);
    }
}
