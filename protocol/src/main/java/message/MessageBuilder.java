package message;

/**
 * Created on 24.04.2017.
 */
public class MessageBuilder {
    private String username;
    private String content;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Message build() {
        return new Message(getUsername(), getContent());
    }
}
