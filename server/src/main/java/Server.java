import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created on 2.04.2017.
 */
public class Server {
    private static final Logger log = Logger.getLogger(Server.class);

    public static void main(String[] args) {
        log.setLevel(Level.DEBUG);
        try {
            Server server = new Server();
            server.start();
        } catch (IOException e) {
            log.error(e);
        }
    }

    private ServerSocket serverSocket;
    List<Socket> connections;

    public Server() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter port");
        int port = scanner.nextInt();
        log.info("Creating server on port " + port);
        serverSocket = new ServerSocket(port);
        connections = new ArrayList<>();
    }

    private void start() throws IOException {
        log.info("Starting server on port " + serverSocket.getLocalPort());
        while (!serverSocket.isClosed()) {
            Socket conn = serverSocket.accept();
            log.info("Accepted new connection on " + conn);
            connections.add(conn);
            ServerThread thread = new ServerThread(this, conn);
            thread.start();
        }
    }

    void broadcast(String message, Socket from) {
        try {
            log.debug("Broadcasting " + message);
            for (Socket conn : connections) {
                if (conn != from) sendResponse(message, conn.getOutputStream());
            }
        } catch (IOException e) {
            log.error(e);
        }
    }

    private void sendResponse(String response, OutputStream outputStream) {
        try {
            log.debug("Sending " + response + " to " + outputStream);
            outputStream.write((response + "\n").getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        } catch (IOException e) {
            log.error(e);
        }
    }
}
