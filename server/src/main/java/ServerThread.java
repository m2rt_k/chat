import converter.Converter;
import message.Message;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class ServerThread extends Thread {
    private static final Logger log = Logger.getLogger(ServerThread.class);
    private final Server server;
    private final Socket socket;
    private String name;

    ServerThread(Server server, Socket socket) {
        this.server = server;
        this.socket = socket;
    }

    public void run() {
        log.info(this + " started with " + socket);
        try(BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8))) {

            name = input.readLine();
            broadcastAsServer(name + " has joined the chat.");

            log.debug("Waiting for input.");
            String in;
            while ((in = input.readLine()) != null) {
                log.debug("Input received: " + in);
                if (!in.trim().equals("")) broadcast(in);
            }
        } catch (Exception e) {
            broadcastAsServer(name + " has left the chat.");
            server.connections.remove(socket);
            log.warn(e);
        }
    }

    private void broadcastAsServer(String message) {
        server.broadcast(Converter.toJson(new Message("SERVER", message)), socket);
    }

    private void broadcast(String message) {
        server.broadcast(message, socket);
    }
}
