import converter.Converter;
import message.Message;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Client {
    private static Socket socket;

    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.print("  ip >");
        String ip = input.next();
        if (ip.equals("d")) ip = "213.168.29.165";
        System.out.print("port >");
        String port = input.next();
        if (port.equals("d")) port = "40000";
        System.out.print("name >");
        String name = input.next();
        socket = new Socket(ip, Integer.parseInt(port));
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        out.write((name+"\n").getBytes(StandardCharsets.UTF_8)); // send name to server
        out.flush();

        Thread thread = new Thread(() -> {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
                String responseString;
                while ((responseString = in.readLine()) != null) {
                    Message message = Converter.fromJson(responseString);
                    System.out.println(message.getUsername() + " : " + message.getContent());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();

        System.out.println("Connected.");

        String ins;
        while (true) {
            ins = input.nextLine();
            if (ins.equals("exit")) System.exit(0);
            out.write((Converter.toJson(new Message(name, ins)) + "\n").getBytes(StandardCharsets.UTF_8));
            out.flush();
        }
    }
}
